﻿using Microsoft.AspNetCore.Http;

namespace VigenereCipher.ViewModels.Home
{
    public class ActionConfiguration
    {
        public CipherType CipherType { get; set; }
        public FileType FileType { get; set; }
        public ActionType ActionType { get; set; }
        public string Key { get; set; }
        public IFormFile File { get; set; }
    }
}
