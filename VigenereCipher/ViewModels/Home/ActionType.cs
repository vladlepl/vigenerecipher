﻿namespace VigenereCipher.ViewModels.Home
{
    public enum ActionType
    {
        Encrypt,
        Decrypt
    }
}
