﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VigenereCipher.Models
{
    public interface ICipher
    {
        string Encode(string text);
        string Decode(string text);
    }
}
