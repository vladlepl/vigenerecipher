﻿using System.IO;

namespace VigenereCipher.Models
{
    public class TxtFile : IFile
    {
        public string Path { get; }

        public TxtFile(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("Файл по указанному пути не найден!");

            this.Path = path;
        }

        public string Read()
        {
            return File.ReadAllText(Path);
        }

        public void Write(string text)
        {
            File.WriteAllText(Path, text);
        }

        public void Dispose() { }
    }
}
