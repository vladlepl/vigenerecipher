﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;

namespace VigenereCipher.Models
{
    public class DocxFile : IFile
    {
        public string Path { get; }

        private Application application;
        private Document document;

        private bool isDisposed = false;

        /// <summary>
        /// Создает экземпляр класса DocxFile
        /// </summary>
        /// <param name="path">АБСОЛЮТНЫЙ путь к файлу</param>
        public DocxFile(string path)
        {
            if (System.IO.Path.GetFullPath(path) != path)
                throw new ArgumentException("Указан неполный путь к файлу!");

            this.Path = path;

            if (!File.Exists(Path))
                throw new FileNotFoundException("Файл по указанному пути не найден!");

            try
            {
                application = new Application();
                application.Documents.Open(path);
                document = application.ActiveDocument;
            }
            catch
            {
                throw new Exception("Не удалось открыть документ!");
            }
        }

        public string Read()
        {
            var text = new StringBuilder();
            foreach (Paragraph par in document.Paragraphs)
                text.Append(par.Range.Text);

            var result = text.ToString();

            if (result.EndsWith("\r"))
                return result.Substring(0, result.Length - 1);
            else
                return result;
        }

        public void Write(string text)
        {
            foreach (Paragraph par in document.Paragraphs)
                par.Range.Text = string.Empty;

            if (document.Paragraphs.Count == 0)
                document.Paragraphs.Add();

            document.Paragraphs[1].Range.Text = text;

            document.Save();
        }

        public void Dispose()
        {
            if (isDisposed)
                return;

            if (document != null)
                document.Close();
            if (application != null)
                application.Quit();

            isDisposed = true;
        }
    }
}
