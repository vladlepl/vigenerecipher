﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VigenereCipher.Models
{
    public interface IFile : IDisposable
    {
        string Path { get; }

        string Read();
        void Write(string text);
    }
}
