﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VigenereCipher.Models
{
    public class VigenereCipher : ICipher
    {
        public string Key { get; }

        private static string alphabet;
        private Dictionary<char, int> charOffset;
        private delegate char GetCharHandler(int index, int offset, int alphabetPower);

        static VigenereCipher()
        {
            alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        }

        public VigenereCipher(string key)
        {
            Key = key.ToLower();

            if (!Key.All(x => alphabet.Contains(x)))
                throw new ArgumentException("Ключ должен состоять только из букв русского алфавита.");

            charOffset = new Dictionary<char, int>(Key.Distinct().Count());
            foreach (var ch in Key.Distinct())
                charOffset.Add(ch, alphabet.IndexOf(ch));
        }

        public string Encode(string text)
        {
            return GetResult(text, (index, offset, alphabetPower) => alphabet[(index + offset) % alphabetPower]);
        }

        public string Decode(string text)
        {
            return GetResult(text, (index, offset, alphabetPower) => alphabet[(index - offset + alphabetPower) % alphabetPower]);
        }

        private string GetResult(string text, GetCharHandler GetChar)
        {
            var result = new StringBuilder(text.Length);
            int keyIterator = 0;

            foreach (var ch in text)
            {
                char lowerChar = char.ToLower(ch);

                if (alphabet.Contains(lowerChar))
                {
                    char currentChar = GetChar(alphabet.IndexOf(lowerChar), charOffset[Key[keyIterator++ % Key.Length]], alphabet.Length);
                    if (char.IsUpper(ch))
                        result.Append(char.ToUpper(currentChar));
                    else
                        result.Append(currentChar);
                }
                else
                {
                    result.Append(ch);
                }
            }

            return result.ToString();
        }
    }
}
