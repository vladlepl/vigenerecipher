﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VigenereCipher.Models;
using VigenereCipher.ViewModels.Home;

namespace VigenereCipher.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["IsErrorReceived"] = false;
            return View(new ActionConfiguration());
        }

        [HttpPost]
        public async Task<IActionResult> Index(ActionConfiguration model, IFormFile uploadFile)
        {
            ViewData["IsErrorReceived"] = false;

            try
            {
                model.File = uploadFile;

                ICipher cipher;
                IFile file;

                if (!Directory.Exists("Uploads"))
                    Directory.CreateDirectory("Uploads");

                string fullPath = Path.Combine(Path.GetFullPath("Uploads"), uploadFile.FileName);

                //var filePath = Path.Combine("Uploads", uploadFile.FileName);
                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    await uploadFile.CopyToAsync(fileStream);
                }

                if (model.FileType == FileType.DOCX)
                    file = new DocxFile(fullPath);
                else if (model.FileType == FileType.TXT)
                    file = new TxtFile(fullPath);
                else
                    throw new ArgumentException("Неверный формат данных!");

                using (file)
                {
                    if (model.CipherType == CipherType.Vigenere)
                        cipher = new Models.VigenereCipher(model.Key);
                    else
                        throw new ArgumentException("Неверный тип алгоритма шифрования!");

                    string result;
                    if (model.ActionType == ActionType.Encrypt)
                        result = cipher.Encode(file.Read());
                    else
                        result = cipher.Decode(file.Read());

                    file.Write(result);
                }

                return PhysicalFile(fullPath, model.FileType == FileType.TXT ? "application/txt" : "application/docx", uploadFile.FileName);

            }
            catch (Exception ex)
            {
                ViewData["IsErrorReceived"] = true;
                ViewData["ErrorMessage"] = ex.Message;

                return View(new ActionConfiguration());
            }
        }
    }
}
