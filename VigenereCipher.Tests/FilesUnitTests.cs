﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VigenereCipher.Models;

namespace VigenereCipher.Tests
{
    [TestClass]
    public class FilesUnitTests
    {
        [TestMethod]
        public void WriteToTXT()
        {
            IFile file = new TxtFile(Path.Combine(Directory.GetCurrentDirectory(), "encodedFile.txt"));

            bool result = IsFileWritingCorrect(file);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void WriteToDOCX()
        {
            IFile file = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "encodedFile.docx"));

            bool result = IsFileWritingCorrect(file);

            Assert.IsTrue(result);
        }

        private bool IsFileWritingCorrect(IFile file)
        {
            using (file)
            {
                string before = file.Read();
                file.Write(before);
                string after = file.Read();
                return before == after;
            }
        }

        [TestMethod]
        public void TxtFileThrowsFileNotFoundExceptionIfFileDoesNotExist()
        {
            bool result = false;
            try
            {
                var txtFile = new TxtFile(Path.Combine(Directory.GetCurrentDirectory(), "someFileWhichDoesntExist.txt"));
            }
            catch (Exception ex)
            {
                result = ex.GetType() == typeof(FileNotFoundException);
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DocxFileThrowsFileNotFoundExceptionIfFileDoesNotExist()
        {
            bool result = false;

            try
            {
                var docxFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "someFileWhichDoesntExist.docx"));
            }
            catch (Exception ex)
            {
                result = ex.GetType() == typeof(FileNotFoundException);
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DocxFileThrowsArgumentExceptionIfPathIsNotFull()
        {
            bool result = false;

            try
            {
                var docxFile = new DocxFile("encodedFile.docx");
            }
            catch (Exception ex)
            {
                result = ex.GetType() == typeof(ArgumentException);
            }

            Assert.IsTrue(result);
        }
    }
}
