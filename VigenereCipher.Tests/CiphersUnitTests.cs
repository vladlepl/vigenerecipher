﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VigenereCipher.Tests
{
    [TestClass]
    public class CiphersUnitTests
    {
        [TestMethod]
        public void VigenereThrowsArgumentExceptionIfKeyContainsNonCyrillicLetters()
        {
            try
            {
                var cipher = new Models.VigenereCipher("keklolarbidol");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
                return;
            }

            Assert.Fail();
        }

        [TestMethod]
        public void VigenereDecode()
        {
            // Arrange
            var cipher = new Models.VigenereCipher("скорпион");
            string encoded = "бщцфаирщри, бл ячъбиуъ щбюэсяёш гфуаа!!!";

            // Act
            string result = cipher.Decode(encoded);

            // Assert
            string expected = "поздравляю, ты получил исходный текст!!!";
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void VigenereEncode()
        {
            // Arrange
            var cipher = new Models.VigenereCipher("скорпион");
            string decoded = "поздравляю, ты получил исходный текст!!!";

            // Act
            string result = cipher.Encode(decoded);

            // Assert
            string expected = "бщцфаирщри, бл ячъбиуъ щбюэсяёш гфуаа!!!";
            Assert.AreEqual(expected, result);
        }

        //[TestMethod]
        //public void IsControllerWorksCorrect()
        //{
        //    // Arrange
        //    var controller = new HomeController();
        //    var config = new ActionConfiguration()
        //    {
        //        ActionType = ActionType.Decrypt,
        //        FileType = FileType.TXT,
        //        CipherType = CipherType.Vigenere,
        //        Key = "скорпион"
        //    };
        //    FormFile file;
        //    using (var fs = new FileStream("encodedFile.txt", FileMode.Open))
        //    {
        //        file = new FormFile(fs, 0, fs.Length, "uploadFile", "uploadFile.txt");
        //    }
        //    var result = controller.Index(config, file);
        //    (result.Result as PhysicalFileResult).

        //    var cipher = new VigenereCipher.Models.VigenereCipher("скорпион");
        //    var decodedFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "decodedFile.docx"));
        //    var expectedFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "encodedFile.docx"));

        //    using (decodedFile)
        //    {
        //        using (expectedFile)
        //        {
        //            // Act
        //            string result = cipher.Encode(decodedFile.Read());

        //            // Assert
        //            Assert.AreEqual(expectedFile.Read(), result);
        //        }
        //    }
        //}
    }
}
