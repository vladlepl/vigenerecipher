﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VigenereCipher.Models;

namespace VigenereCipher.Tests
{
    [TestClass]
    public class CiphersFilesUnitTests
    {
        [TestMethod]
        public void VigenereDecodeTXT()
        {
            // Arrange
            var cipher = new Models.VigenereCipher("скорпион");
            var encodedFile = new TxtFile("encodedFile.txt");
            var expectedFile = new TxtFile("decodedFile.txt");

            // Act
            string result = cipher.Decode(encodedFile.Read());

            // Assert
            Assert.AreEqual(expectedFile.Read(), result);
        }

        [TestMethod]
        public void VigenereEncodeTXT()
        {
            // Arrange
            var cipher = new Models.VigenereCipher("скорпион");
            var decodedFile = new TxtFile("decodedFile.txt");
            var expectedFile = new TxtFile("encodedFile.txt");

            // Act
            string result = cipher.Encode(decodedFile.Read());

            // Assert
            Assert.AreEqual(expectedFile.Read(), result);
        }

        [TestMethod]
        public void VigenereDecodeDOCX()
        {
            // Arrange
            var cipher = new Models.VigenereCipher("скорпион");
            var encodedFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "encodedFile.docx"));
            var expectedFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "decodedFile.docx"));

            using (encodedFile)
            {
                using (expectedFile)
                {
                    // Act
                    string result = cipher.Decode(encodedFile.Read());

                    // Assert
                    Assert.AreEqual(expectedFile.Read(), result);
                }
            }
        }

        [TestMethod]
        public void VigenereEncodeDOCX()
        {
            // Arrange
            var cipher = new Models.VigenereCipher("скорпион");
            var decodedFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "decodedFile.docx"));
            var expectedFile = new DocxFile(Path.Combine(Directory.GetCurrentDirectory(), "encodedFile.docx"));

            using (decodedFile)
            {
                using (expectedFile)
                {
                    // Act
                    string result = cipher.Encode(decodedFile.Read());

                    // Assert
                    Assert.AreEqual(expectedFile.Read(), result);
                }
            }
        }
    }
}
