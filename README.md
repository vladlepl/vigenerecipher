## О проекте
Проект создан с использованием ASP.NET MVC и предоставляет следующие функциональные возможности:  

1. Выбор **алгоритма шифрования**:  
- шифр Виженера;  
- ... ну и все, хватит пока).  
2. Выбор **типа файла**:  
- текстовый (.txt);  
- документ word (.docx).  
3. Ввод **ключа** для алгоритма.  
4. Выбора действия:  
- зашифровать файл;  
- дешифровать файл.  
5. **Загрузка файла** с данными с устройства пользователя (только разрешенные типы файлов).  
6. **Получение преобразованного файла** по заданным настройкам в стандартную директорию.  

Проект содержит Unit-тесты для алгоритма шифрования, работы с файлами и работы с файлами и алгоритмами вместе.

---

## Getting started
*Для корректной работы ПО необходимо иметь на своем устройстве (по идее, на сервере) Microsoft Office (для работы с .docx файлами).*  

Итак, если после танцев с бубнов у Вас все-таки запустилась программа, то вы можете (нет) выбрать алгоримт шифрования и тип файла, затем ввести ключ и загрузить файл. После выполнения всех этих нехитрых манипуляций можно нажимать на кнопку "Получить за/расшифрованный файл" и в течение некоторого времени готовый файл будет загружен на Ваше устройство.

Примечание:  
- Ключ должен состоять только из кириллицы!  
- Если при выборе файла указать файл неверного типа (это возможно, если в окне выбора файла выбрать пункт "All files"), то такой файл загружен не будет (даже не пытайтесь... пожалуйста).  
- При работе с текстовыми файлами (.txt) используется кодировка UTF-8, поэтому если будет загружен файл с другой кодировкой - получите не то, что нужно).